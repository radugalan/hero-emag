<?php

/**
 * Created by PhpStorm.
 * User: radugalan
 * Date: 23/11/2016
 * Time: 20:14
 */
class Creature
{
    private $name;
    private $health;
    private $strength;
    private $defense;
    private $speed;
    private $luck;
    private $attacks = 0;
    private $defenses = 0;
    private $fullDefenses = 0;
    private $usedFullDefensesSkill = false;
    public function attack(Creature $creature, $canDoubleAttack = true){
        $damage = $this->strength - $creature->getDefense(); //replace this with 1 to see a full battle.
        $defended = $creature->defend();
        if($canDoubleAttack)
            $doubleAttack = $this->canDoubleAttack();
        else
            $doubleAttack = false;
        if($defended) $damage = $damage/2;
        $fullDefend = $creature->fullDefend();
        if($fullDefend != true)
            $creature->attacked($damage);
        $this->attacks++;
        if($doubleAttack){
            $oldHealth = $creature->getHealth();
            $stringDoubleAttack = $this->attack($creature,false);
        }
        if($fullDefend)
            return $this->name.' a avut ghinion, l-a facut ranit cu 0 pe '.$creature->getName().'.'.($doubleAttack ? $stringDoubleAttack : '').'<br>'.PHP_EOL;
        return $this->name . ' l-a atacat pe '.$creature->getName().' . L-a ranit cu '.$damage.'. '.$creature->getName().' mai are '.($doubleAttack?$oldHealth:$creature->getHealth()).' viata.'.($defended ?$creature->getName().' si-a folosit apararea si a primit jumatate din damage':'').($doubleAttack ? $this->name. ' si-a folosit aptitudinea de dublu atac si a atacat de 2 ori: '. $stringDoubleAttack:'').'<br>'.PHP_EOL;
    }
    public function getAttacks(){
        return $this->attacks;
    }
    public function canDoubleAttack(){
        return false;
    }
    public function resetAttacks(){
        $this->attacks = 0;
    }
    public function defend(){
        $this->defenses++;
        return false;
    }
    public function increaseDefenses(){
        $this->defenses++;
    }
    public function increaseAttacks(){
        $this->attacks++;
    }
    public function getDefenses(){
        return $this->defenses;
    }
    public function resetDefenses(){
        $this->defenses = 0;
    }
    public function attacked($damage){
        $this->health = $this->health - $damage;
    }
    public function getName(){
        return $this->name;
    }
    public function getHealth(){
        return $this->health;
    }
    public function getStrength(){
        return $this->strength;
    }
    public function getDefense(){
        return $this->defense;
    }
    public function getSpeed(){
        return $this->speed;
    }
    public function getLuck(){
        return $this->luck;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function setHealth($health){
        $this->health = $health;
    }
    public function setStrength($strength){
        $this->strength = $strength;
    }
    public function setDefense($defense){
        $this->defense = $defense;
    }
    public function setSpeed($speed){
        $this->speed = $speed;
    }
    public function setLuck($luck){
        $this->luck = $luck;
    }
    public function printProperties(){
        echo $this->getName().' are urmatoarele proprietati: Viata: '.$this->getHealth().', Putere: '. $this->getStrength().', Aparare: '.$this->getDefense().', Viteza: '.$this->getSpeed().', Noroc: '.$this->getLuck().'<br>'.PHP_EOL;
    }
    public function fullDefend(){
        $this->fullDefenses++;
        $lucky = rand(1,intval($this->luck/10));
        $return = false;
        if(!$this->usedFullDefensesSkill){
            if($this->fullDefenses == $lucky){
                $return = true;
                $this->usedFullDefensesSkill = true;
            } else if($this->fullDefenses == (10 - $this->luck/10)){
                $return = true;
                $this->fullDefenses = 0;
                $this->usedFullDefensesSkill = false;
            }
        } else if($this->fullDefenses >= (10 - $this->luck/10)){
            $this->fullDefenses = 0;
            $this->usedFullDefensesSkill = false;
        }
         return $return;
    }
}