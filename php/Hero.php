<?php

/**
 * Created by PhpStorm.
 * User: radugalan
 * Date: 23/11/2016
 * Time: 20:18
 */
require_once('Creature.php');
class Hero extends Creature
{
    private $usedDefenseSkill = false;
    private $usedAttackSkill = false;

    public function __construct(){
        $this->setHealth (rand(70,100));
        $this->setStrength(rand(70,80));
        $this->setDefense(rand(45,55));
        $this->setSpeed(rand(40,50));
        $this->setLuck(rand(10,30));
        $this->setName('Orderus');
    }

    public function canDoubleAttack(){
        $this->increaseAttacks();
        $lucky = rand(1,10);
        $return = false;
        if(!$this->usedAttackSkill){
            if($this->getAttacks() == $lucky) {
                $return = true;
                $this->usedAttackSkill = true;
            } else if($this->getAttacks() == 10){
                $return = true;
                $this->resetAttacks();
                $this->usedAttackSkill = false;
            }
        } else if($this->getAttacks() >= 10){
            $this->resetAttacks();
            $this->usedAttackSkill = false;
        }
        return $return;
    }

    public function defend()
    {
        $this->increaseDefenses();
        $lucky = rand(1,5);
        $return = false;
        if (!$this->usedDefenseSkill) {
            if ($this->getDefenses() == $lucky) {
                $return = true;
                $this->usedDefenseSkill = true;
            } else if ($this->getDefenses() == 5) {
                $return = true;
                $this->resetDefenses();
                $this->usedDefenseSkill = false;
            }
        } else if ($this->getDefenses() >= 5) {
            $this->resetDefenses();
            $this->usedDefenseSkill = false;
        }

        return $return;
    }
}