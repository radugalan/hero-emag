<?php

/**
 * Created by PhpStorm.
 * User: radugalan
 * Date: 23/11/2016
 * Time: 21:40
 */
require_once 'Beast.php';
require_once 'Creature.php';
require_once 'Hero.php';
class Fight
{
    var $beast = null;
    var $hero = null;
    var $attacker = null;
    var $defender = null;
    var $rounds = 0;
    var $fightOver = false;
    public function __construct()
    {
        $this->beast = new Beast();
        $this->hero = new Hero();
        echo 'Bine ati venit in eMagia. Eroul nostru care da multe comenzi se plimba prin acest taram fermecat. Cand deodata se intalneste cu o bestie. Acestia se privesc indelung in ochi si incep o batalie.<br>';
        echo $this->beast->printProperties();
        echo $this->hero->printProperties();
    }

    public function firstRound(){
        if($this->hero->getSpeed() == $this->beast->getSpeed() ){
            if($this->hero->getLuck() > $this->beast->getLuck()){
                $this->attacker = $this->hero;
                $this->defender = $this->beast;
            } else {
                $this->attacker = $this->beast;
                $this->defender = $this->hero;
            }

        } else if($this->hero->getSpeed() > $this->beast->getSpeed()){
            $this->attacker = $this->hero;
            $this->defender = $this->beast;
        } else {
            $this->attacker = $this->beast;
            $this->defender = $this->hero;
        }

        echo $this->attacker->attack($this->defender);
        $oldAttacker = clone $this->attacker;
        $this->attacker = $this->defender;
        $this->defender = $oldAttacker;
        $this->rounds=1;
    }
    public function newRound(){
        $this->rounds++;
        echo $this->attacker->attack($this->defender);
        $oldAttacker = clone $this->attacker;
        $this->attacker = $this->defender;
        $this->defender = $oldAttacker;
        if($this->attacker->getHealth() <= 0) {
            echo $this->attacker->getName().' a murit.<br>'.PHP_EOL;
            $this->fightOver = true;
        }
        if($this->defender->getHealth() <= 0) {
            echo $this->defender->getName().' a murit.<br>'.PHP_EOL;
            $this->fightOver = true;
        }
        if($this->rounds == 20) {
            echo 'Lupta s-a terminat, ambele creaturi sunt sleite de puteri si nu mai pot continua, s-au pus la somn si reiau batalia dupa ce isi revin.<br>'.PHP_EOL;
            $this->fightOver = true;
        }
    }
}