<?php

/**
 * Created by PhpStorm.
 * User: radugalan
 * Date: 23/11/2016
 * Time: 21:28
 */
require_once('Creature.php');
class Beast extends Creature
{
    public function __construct()
    {
        $this->setHealth (rand(60,90));
        $this->setStrength(rand(60,90));
        $this->setDefense(rand(40,60));
        $this->setSpeed(rand(40,60));
        $this->setLuck(rand(25,40));
        $this->setName('Beast');
    }
}